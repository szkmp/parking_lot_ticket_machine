class CreateParkingStalls < ActiveRecord::Migration
  def change
    create_table :parking_stalls do |t|
      t.boolean :occupied

      t.timestamps null: false
    end
  end
end
