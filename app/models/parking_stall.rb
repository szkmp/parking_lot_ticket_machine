class ParkingStall < ActiveRecord::Base
  has_one :car

  before_save :update_vacancy

  def vacant?
    !occupied
  end

  private

  def update_vacancy
    self.occupied = has_car?
  end

  def has_car?
    car.present?
  end
end
