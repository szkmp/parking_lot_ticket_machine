require 'forwardable'

class CarPark
  attr_reader :parking_stalls

  extend Forwardable


  delegate %w(size) => :parking_stalls

  def parking_stalls
    @parking_stalls ||= []
  end

  def initialize
    @parking_stalls = ParkingStall.all
  end

  def generate_parking_stalls(count: 1)
    ParkingStall.destroy_all
    Car.destroy_all
    ActiveRecord::Base.connection.execute("delete from sqlite_sequence where name='parking_stalls'")
    @parking_stalls = []

    count.times {
      parking_stalls << ParkingStall.create(occupied: false)
    }
  end

  def allocate(car)
    if full?
      raise AlreadyFullError
    else
      parking_stalls.each {|parking_stall|
        if parking_stall.vacant?
          parking_stall.update_attribute(:car, car)
          return parking_stall
        end
      }
    end
  end

  def deallocate(parking_stall_id)
    parking_stalls[(parking_stall_id - 1)].update_attribute(:car, nil)
  end

  def allocated_parking_stalls
    parking_stalls.select{|parking_stall| parking_stall.occupied? }
  end

  def parking_stalls_for_color(color)
    Enumerator.new do |yeilder|
      parking_stalls.each { |parking_stall|
        if parking_stall.car.try(:color) == color
          yeilder << parking_stall
        end
      }
    end
  end

  def parking_stall_for_registration_number(registration_number)
    parking_stalls.detect {|parking_stall|
      parking_stall.car.try(:registration_number) == registration_number
    }
  end

  def full?
    parking_stalls.all? {|parking_stall| parking_stall.occupied? }
  end

  class AlreadyFullError < StandardError; end
end
