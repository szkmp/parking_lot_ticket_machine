require 'spec_helper'

describe CarPark do
  describe "#generate_parking_stalls" do
    let(:n) { 6 }
    let(:car_park) do
      CarPark.new.tap{ |car_park|
        car_park.generate_parking_stalls(count: n)
      }
    end

    it "creates specified number of parking stalls" do
      expect(car_park.size).to eq(n)
    end
  end

  describe "#allocate" do
    let(:car_park) do
      CarPark.new.tap{ |car_park|
        car_park.generate_parking_stalls(count: 6)
      }
    end
    let(:car) { Car.make }

    before do
      car_park.allocate(car)
    end

    it "allocates a car" do
      expect(car_park.parking_stalls.map(&:car)).to include(car)
    end

    it "returns id of allocated parking_stall" do
      allocated = car_park.allocate(car)
      expect(car_park.allocated_parking_stalls).to include(allocated)
    end
  end

  describe "#deallocate" do
    let(:car_park) do
      CarPark.new.tap{ |car_park|
        car_park.generate_parking_stalls(count: 6)
      }
    end
    let(:car) { Car.make }

    before do
      allocated_parking_stall = car_park.allocate(car)
      car_park.deallocate(allocated_parking_stall.id)
    end

    it "deallocates a car" do
      expect(car_park.parking_stalls.map(&:car)).not_to include(car)
    end
  end
end
