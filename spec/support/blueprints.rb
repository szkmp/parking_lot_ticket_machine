require 'machinist/active_record'

Car.blueprint do
  registration_number { sn }
  color { ['white', 'red', 'blue'].sample }
end

ParkingStall.blueprint do
end
