require 'spec_helper'
require 'ticket_machine'

describe TicketMachine do
  let(:ticket_machine) { TicketMachine.new }

  describe "#create_parking_lot" do
    let(:car_park) { CarPark.new }

    before do
      allow(CarPark).to receive(:new).and_return(car_park)
    end

    it "calls CarPark#generate_parking_stalls" do
      expect(car_park).to receive(:generate_parking_stalls)
      ticket_machine.create_parking_lot([1])
    end
  end

  describe "#park" do
    let(:car) { Car.make! }

    before do
      ticket_machine.create_parking_lot([1])
    end

    it "prints allocated parking_stall_id" do
      expected = "Allocated slot number: 1"
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.park([car.registration_number, car.color])
    end

    context "when car_park is already full" do
      before do
        ticket_machine.park([car.registration_number, car.color])
      end

      it "prints error message" do
        expected = "Sorry, parking lot is full"
        expect(STDOUT).to receive(:puts).with(expected)
        ticket_machine.park([car.registration_number, car.color])
      end
    end
  end

  describe "#leave" do
    let(:car) { Car.make! }
    let(:parking_stall_id) { 1 }

    before do
      ticket_machine.create_parking_lot([6])
      ticket_machine.park([car.registration_number, car.color])
    end

    it "prints deallocated parking_stall_id" do
      expected = "Slot number #{parking_stall_id} is free"
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.leave([parking_stall_id])
    end
  end

  describe "#status" do
    let(:cars) do
      2.times.map{ Car.make }
    end

    before do
      ticket_machine.create_parking_lot([6])
      cars.each do |car|
        ticket_machine.park([car.registration_number, car.color])
      end
    end

    it "prints status" do
      expected = %Q(Slot No.    Registration No    Colour
1    #{cars.first.registration_number}    #{cars.first.color}
2    #{cars.second.registration_number}    #{cars.second.color})
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.status
    end

  end

  describe "#slot_numbers_for_cars_with_colour" do
    let(:white_car) do
      Car.make(color: 'white')
    end
    let(:cars) do
      [Car.make(color: 'red'), white_car]
    end

    before do
      ticket_machine.create_parking_lot([6])
      cars.each do |car|
        ticket_machine.park([car.registration_number, car.color])
      end
    end

    it "prints slot numbers for specified colour of car" do
      expected = '2'
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.slot_numbers_for_cars_with_colour(['white'])
    end
  end

  describe "#registration_numbers_for_cars_with_colour" do
    let(:white_cars) do
      [Car.make(color: 'white'), Car.make(color: 'white')]
    end
    let(:cars) do
      [Car.make(color: 'red'), *white_cars]
    end

    before do
      ticket_machine.create_parking_lot([6])
      cars.each do |car|
        ticket_machine.park([car.registration_number, car.color])
      end
    end

    it "prints registration numbers of cars for specified colour" do
      expected = white_cars.map(&:registration_number).join(', ')
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.registration_numbers_for_cars_with_colour(['white'])
    end
  end

  describe "#slot_number_for_registration_number" do
    let(:expected_car) do
      Car.make
    end
    let(:cars) do
      [Car.make, expected_car]
    end

    before do
      ticket_machine.create_parking_lot([6])
      cars.each do |car|
        ticket_machine.park([car.registration_number, car.color])
      end
    end

    it "prints slot number for specified registration number" do
      expected = 2
      expect(STDOUT).to receive(:puts).with(expected)
      ticket_machine.slot_number_for_registration_number([expected_car.registration_number])
    end
  end
end
