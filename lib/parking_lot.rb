require File.expand_path('../../config/environment',  __FILE__)
require 'ticket_machine'

if ARGV.size >= 1
  TicketMachine.read_file
else
  TicketMachine.start_console
end
