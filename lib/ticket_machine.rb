require 'highline'
require 'cli-console'
require 'highline_like_file_io'

# Represents ticket machine in a car park, which handles parking stalls and issues tickets for cars
#
class TicketMachine
  attr_reader :car_park

  def initialize
    @car_park = CarPark.new
  end

  # Methods defined in this modules are used as commands in CLI
  module PublicCommands
    extend CLI::Task

    MESSAGE_NOT_FOUND = "Not found"

    def create_parking_lot(params)
      car_park.generate_parking_stalls(count: params.first.to_i)

      puts "Created a parking lot with #{car_park.parking_stalls.size} slots"
    end

    def park(params)
      car = Car.new(registration_number: params.first, color: params.second)
      allocated_parking_stall = car_park.allocate(car)

      puts "Allocated slot number: #{allocated_parking_stall.id}"
    rescue CarPark::AlreadyFullError
      puts "Sorry, parking lot is full"
    end

    def leave(params)
      parkring_stall_id = params.first.to_i
      car_park.deallocate(parkring_stall_id)

      puts "Slot number #{parkring_stall_id} is free"
    end

    def status(*params)
      outputs = ["Slot No.    Registration No    Colour"]

      car_park.parking_stalls.each do |ps|
        if ps.occupied?
          outputs << "#{ps.id}    #{ps.car.registration_number}    #{ps.car.color}"
        end
      end

      puts outputs.join("\n")
    end

    def slot_numbers_for_cars_with_colour(params)
      founds = car_park.parking_stalls_for_color(params.first)
      ids = founds.each_with_object([]) { |parking_stall, sum|
          sum << parking_stall.id
        }

      puts(ids.join(", ").presence || MESSAGE_NOT_FOUND)
    end

    def registration_numbers_for_cars_with_colour(params)
      founds = car_park.parking_stalls_for_color(params.first)
      registration_numbers = founds.each_with_object([]) { |parking_stall, sum|
          if parking_stall.occupied?
            sum << parking_stall.car.registration_number
          end
        }
      puts(registration_numbers.join(", ").presence || MESSAGE_NOT_FOUND)
    end

    def slot_number_for_registration_number(params)
      found = car_park.parking_stall_for_registration_number(params.first)

      puts(found.try(:id).presence || MESSAGE_NOT_FOUND)
    end
  end

  include PublicCommands

  def self.start_console
    io = HighLine.new
    console = CLI::Console.new(io)
    ticket_machine = TicketMachine.new

    puts "Available commands;"
    TicketMachine::PublicCommands.public_instance_methods.each do |command|
      puts command
      console.addCommand(command.to_s, ticket_machine.method(command))
    end

    # Add common commands
    console.addHelpCommand('help', 'Help')
    console.addExitCommand('exit', 'Exit from program')
    console.addAlias('quit', 'exit')

    console.start("%s> ", ["parking_lot"])
  end

  def self.read_file
    io = ARGF.to_io
    io.extend HighlineLikeFileIo

    console = CLI::Console.new(io)
    ticket_machine = TicketMachine.new

    TicketMachine::PublicCommands.public_instance_methods.each do |command|
      console.addCommand(command.to_s, ticket_machine.method(command))
    end

    console.addExitCommand('exit', 'Exit from program')
    console.start("%s> ", ["parking_lot"])
  end
end
