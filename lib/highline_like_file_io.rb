# This module is a monkey patch for making File IO work well with a gem called CLI-Console
#
module HighlineLikeFileIo
  def indent_level
    1
  end

  def indent_level=(i)
  end

  def ask(template_or_question, answer_type = nil, &details)
    if eof?
      "exit"
    else
      line = readline.gsub("\n", "")
      line if line.present?
    end
  end

  def say(statement)
    STDOUT.puts statement
  end
end
